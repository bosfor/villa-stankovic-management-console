package com.villastankovic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.villastankovic.model.Manager;
import com.villastankovic.service.ManagerService;

@RestController
public class LoginController {
	
	@Autowired
	private ManagerService service;
	
	@CrossOrigin
	@PostMapping("/login")
	public Manager loginUser (@RequestBody Manager manager) throws Exception {
		String tempEmail = manager.getEmail();
		String tempPass = manager.getPassword();
		Manager userObj = null;
		if(tempEmail != null && tempPass != null) {
			userObj = service.fetchManagerByEmailIdAndPassword(tempEmail, tempPass);
		}
		if(userObj == null) {
			throw new Exception("Bad credentials");
		}
		return userObj;
	}

}
