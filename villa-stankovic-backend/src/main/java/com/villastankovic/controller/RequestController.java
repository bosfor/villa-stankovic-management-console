package com.villastankovic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.villastankovic.exception.ResourceNotFoundException;
import com.villastankovic.model.Request;
import com.villastankovic.repository.RequestRepository;
import com.villastankovic.service.RequestService;

@RestController
@RequestMapping("/requests")
public class RequestController {
	
	@Autowired
	RequestRepository requestRepository;
		
	RequestService requestService;
		
	@Autowired
	public RequestController(RequestService requestService) {
		this.requestService = requestService;
	}

	@CrossOrigin
	@GetMapping("/all")
	public List<Request> getAll(){
		return (List<Request>) requestRepository.getAllDESC();	
	}
	
	@CrossOrigin
	@GetMapping("/highlighted")
	public List<Request> getHighlighted(){	
		return (List<Request>) requestRepository.getHighlighted();	
	}
	
	@CrossOrigin
	@PutMapping("/highlight/{id}")
	public ResponseEntity<Request> highlight( @PathVariable int id, @RequestBody Request requestDetails) throws ResourceNotFoundException {
		Request request = requestRepository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ id));
		request.setOption(true);
		final Request updatedRequest = requestRepository.save(request);
		return ResponseEntity.ok(updatedRequest);
	}
	
	@CrossOrigin
	@PutMapping("/unhighlight/{id}")
	public ResponseEntity<Request> unhighlight( @PathVariable int id, @RequestBody Request requestDetails) throws ResourceNotFoundException {
		Request request = requestRepository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ id));
		request.setOption(false);
		final Request updatedRequest = requestRepository.save(request);
		return ResponseEntity.ok(updatedRequest);
	}

	@CrossOrigin
	@DeleteMapping("/{id}")
	public void deleteRequest(@PathVariable Request id) {
		requestRepository.delete(id);		
	}

}
