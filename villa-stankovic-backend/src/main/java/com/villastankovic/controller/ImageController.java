package com.villastankovic.controller;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.villastankovic.exception.ResourceNotFoundException;
import com.villastankovic.model.Image;
import com.villastankovic.model.Manager;
import com.villastankovic.repository.ImageRepository;
import com.villastankovic.service.ManagerService;



@RestController
@RequestMapping(path = "image")
public class ImageController {
	
	@Autowired
	ImageRepository imageRepository;
	
	@Autowired
	private ManagerService service;
	
	@CrossOrigin
	@PostMapping("/upload")
	public void uplaodImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		Manager manager = service.getById(Long.parseLong(file.getOriginalFilename()));
		Image img = new Image(null, Long.parseLong(file.getOriginalFilename()), file.getOriginalFilename(), file.getContentType(),
				compressBytes(file.getBytes()), manager);
		imageRepository.save(img);
	}
	
	@CrossOrigin
	@PutMapping("/update")
	public void updateImage(@RequestParam("imageFile") MultipartFile file) throws IOException, ResourceNotFoundException {
		Image im = imageRepository.findByName(file.getOriginalFilename())
				.orElseThrow(() -> new ResourceNotFoundException("Image not found"));
		System.out.println("Original Image Byte Size - " + file.getBytes().length);
		im.setName(file.getOriginalFilename());
		im.setPicByte(compressBytes(file.getBytes()));
		im.setType(file.getContentType());
		final Image updatedImageModel = imageRepository.save(im);
	}
	
	@CrossOrigin
	@GetMapping(path = { "/get/{imageName}" })
	public Image getImage(@PathVariable("imageName") String imageName) throws IOException {
		final Optional<Image> retrievedImage = imageRepository.findByName(imageName);
		Image img = new Image(null, retrievedImage.get().getManagerId() , retrievedImage.get().getName(), retrievedImage.get().getType(),
				decompressBytes(retrievedImage.get().getPicByte()), retrievedImage.get().getManager());
		return img;
	}
	
	@CrossOrigin
	@GetMapping(path = { "/get/all" })
	public List<Image> getImages() throws IOException {
		final Image retrievedImage = (Image) imageRepository.findAll();
		Image img = new Image(null, retrievedImage.getManagerId() , retrievedImage.getName(), retrievedImage.getType(),
				decompressBytes(retrievedImage.getPicByte()), retrievedImage.getManager());
		return (List<Image>) img;
	}
	
	// compress the image bytes before storing it in the database
	public static byte[] compressBytes(byte[] data) {
		Deflater deflater = new Deflater();
		deflater.setInput(data);
		deflater.finish();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		while (!deflater.finished()) {
			int count = deflater.deflate(buffer);
			outputStream.write(buffer, 0, count);
		}
		try {
			outputStream.close();
		} catch (IOException e) {
		}
		System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
		return outputStream.toByteArray();
	}
	
	// uncompress the image bytes before returning it to the angular application
	public static byte[] decompressBytes(byte[] data) {
		Inflater inflater = new Inflater();
		inflater.setInput(data);
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
		byte[] buffer = new byte[1024];
		try {
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
		} catch (IOException ioe) {
		} catch (DataFormatException e) {
		}
		return outputStream.toByteArray();
	}
}