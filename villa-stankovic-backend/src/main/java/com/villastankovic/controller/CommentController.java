package com.villastankovic.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.villastankovic.model.Comment;
import com.villastankovic.service.CommentService;



@RestController
@RequestMapping(path = "comments")
public class CommentController {
	
	
	private CommentService commentService;
	
	@Autowired
	public CommentController(CommentService commentService) {
		this.commentService = commentService;
	}

	@CrossOrigin
	@GetMapping("/all")
	public Iterable<Comment> getAllComments() {
		return commentService.listComments();
	}
	
	@CrossOrigin
	@GetMapping("/post/{id}")
	public List<Comment> getAllCommentsForPostByPostId(@PathVariable(value = "id") Integer id) {
		return commentService.allCommentsForPostByPostId(id);
	}
	
	@CrossOrigin
	@PostMapping("/create")
	public Comment newComment(@RequestBody Comment comment )  {
		return commentService.createComment(comment);
	}
	
	@CrossOrigin
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id ) {
		commentService.deleteComment(id);
		
	}
	


}
