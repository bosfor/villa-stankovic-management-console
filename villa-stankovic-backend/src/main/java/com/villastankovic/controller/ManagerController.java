package com.villastankovic.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.villastankovic.exception.ResourceNotFoundException;
import com.villastankovic.model.Manager;
import com.villastankovic.repository.ManagerRepository;
import com.villastankovic.service.ManagerService;

@RestController
@RequestMapping("/manager")
public class ManagerController {
	
	@Autowired
	private ManagerService service;
	
	ManagerRepository mangerRepository;

	@Autowired
	public ManagerController(ManagerRepository mangerRepository) {
		this.mangerRepository = mangerRepository;
	}
	
//	@CrossOrigin
//	@GetMapping(produces = "application/json")
//	@RequestMapping({ "/validateLogin" })
//	public User validateLogin() {
//		return new User("User successfully authenticated");
//	}
	
	@CrossOrigin
	@GetMapping("/all")
	private List<Manager> allAdmind(){
		return (List<Manager>) mangerRepository.findAll();	
	}
	
	@CrossOrigin
	@DeleteMapping(value = "/{id}")
	public void deleteUser(@PathVariable Manager id) {
		mangerRepository.delete(id);		
	}
	
	@CrossOrigin
	@PutMapping("/admin/{id}")
	public ResponseEntity<Manager> admin( @PathVariable Long id, @RequestBody Manager managerDetails) throws ResourceNotFoundException {
		Manager manager = mangerRepository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ id));
		manager.setOption(true);
		final Manager updatedManager = mangerRepository.save(manager);
		return ResponseEntity.ok(updatedManager);
	}
	
	@CrossOrigin
	@PostMapping("/registeruser")
	public Manager createManager(@RequestBody Manager manager) {
		return mangerRepository.save(manager);
	} 
	
	@CrossOrigin
	@PostMapping("/register")
	public Manager registerUser(@RequestBody Manager manager ) throws Exception {
		String tempEmail = manager.getEmail();
		if(tempEmail != null &&  !"".equals(tempEmail)) {
			Manager userObj = service.fetchManagerByEmail(tempEmail);
				if(userObj != null) {
					throw new Exception("User " + tempEmail + " is already exist");
				}
		}
		Manager managerObj = null;
		managerObj = service.saveManager(manager);
		return managerObj;
	}
	
	@CrossOrigin
	@PutMapping("/unadmin/{id}")
	public ResponseEntity<Manager> undo( @PathVariable Long id, @RequestBody Manager managerDetails) throws ResourceNotFoundException {
		Manager manager = mangerRepository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ id));
		manager.setOption(false);
		final Manager updatedManager = mangerRepository.save(manager);
		return ResponseEntity.ok(updatedManager);
	}
	
	@CrossOrigin
	@GetMapping("/{email}")
	public Manager getManagerByEmailId(@PathVariable(value = "email") String email) {
		Manager manager = service.fetchManagerByEmail(email);	
		return manager;
	}
	
	@CrossOrigin
	@GetMapping("/id/{id}")
	public Manager getById(@PathVariable(value = "id") Long id) {
		Manager manager = service.getById(id);	
		return manager;
	}

	@CrossOrigin
	@PutMapping("/update/{id}")
	public ResponseEntity<Manager> updateUser(@PathVariable Long id, @RequestBody Manager managerDetails) throws ResourceNotFoundException {
		Manager manager = mangerRepository.findById(id)
		.orElseThrow(() -> new ResourceNotFoundException("User not found on :: "+ id));
		manager.setEmail(managerDetails.getEmail());
		manager.setUsername(managerDetails.getUsername());
		manager.setFirst_name(managerDetails.getFirst_name());
		manager.setLast_name(managerDetails.getLast_name());
		manager.setPassword(managerDetails.getPassword());
		final Manager updatedManager = mangerRepository.save(manager);
		return ResponseEntity.ok(updatedManager);
	}
}
