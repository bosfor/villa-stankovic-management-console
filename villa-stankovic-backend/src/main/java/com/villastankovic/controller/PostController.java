package com.villastankovic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.villastankovic.model.Post;
import com.villastankovic.service.PostService;

import java.util.List;
import java.util.Optional;


@RestController
public class PostController {
 
    @Autowired
    private PostService postService;
    
    @CrossOrigin
    @GetMapping("/posts")
    public List<Post> listPosts() {
        return (List<Post>) postService.findAllDESC();
    }
  
    @CrossOrigin
    @GetMapping("/posts/{id}")
    public Optional<Post> findPostById(@PathVariable Long id ) {
        return postService.findOne(id);
    }
       
    @CrossOrigin
    @PostMapping("/posts")
    public Post createPost(@RequestBody Post post) {
    	return postService.create(post);
    }
    
    @CrossOrigin
    @DeleteMapping("/posts/{id}")
    public void delwtePost(@PathVariable Long id) {
    	postService.deletePost(id);
    }
}