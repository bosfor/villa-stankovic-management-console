package com.villastankovic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.villastankovic.model.Manager;
import com.villastankovic.repository.ManagerRepository;


@Service
public class ManagerService {

	@Autowired
	private ManagerRepository managerRepository;
		
	public Manager fetchManagerByEmail(String email) {
		return managerRepository.findByEmail(email);
	}
	
	public Manager fetchManagerByEmailIdAndPassword(String email, String password) {
		return managerRepository.findByEmailAndPassword(email, password);
	}

	public Manager saveManager(Manager manager) {
		return managerRepository.save(manager);
	}

	public Manager getById(Long id) {
		return managerRepository.findUserById(id);
	}

}