package com.villastankovic.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.villastankovic.model.Post;
import com.villastankovic.repository.PostRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PostService {


    private PostRepository postRepository;

    @Autowired
    public PostService(PostRepository postRepository) {
        this.postRepository = postRepository;
    }

	public Optional<Post> findOne(Long id) {
		return postRepository.findById(id);
	}

	public List<Post> findAll() {
		return (List<Post>) postRepository.findAll();
	}

	public Post create(Post post) {
		return postRepository.save(post);
	}

	public List<Post> findAllDESC() {
		return (List<Post>) postRepository.findAllDESC();
	}

	public void deletePost(Long id) {
		postRepository.deleteById(id);
	}

}