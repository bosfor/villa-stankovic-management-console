package com.villastankovic.service;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.villastankovic.model.Comment;
import com.villastankovic.repository.CommentRepository;



@Service
public class CommentService {

	private CommentRepository commentRepository;
	
	@Autowired
	public CommentService(CommentRepository commentRepository) {
		this.commentRepository = commentRepository;
	}

	public Iterable<Comment> listComments() {
		return commentRepository.findAll() ;
	}

	public Comment createComment(Comment comment) {
		return commentRepository.save(comment);
	}

	public List<Comment> allCommentsForPostByPostId(Integer id) {
		return commentRepository.findAllByPostId(id);
	}

	public void deleteComment(Long id) {
		commentRepository.deleteById(id);
	}

	
}
