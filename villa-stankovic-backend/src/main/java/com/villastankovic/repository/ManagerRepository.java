package com.villastankovic.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.villastankovic.model.Manager;

@CrossOrigin
public interface ManagerRepository extends CrudRepository<Manager, Long> {

	public Manager findByEmail(String email);

	public Manager findByEmailAndPassword(String email, String password);

	public Manager findUserById(Long id);

	public Optional<Manager> findById(Long id);

}
