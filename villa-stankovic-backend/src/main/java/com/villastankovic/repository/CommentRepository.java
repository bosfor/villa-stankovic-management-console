package com.villastankovic.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import com.villastankovic.model.Comment;


public interface CommentRepository extends JpaRepository<Comment, Long> {

	List<Comment> findAllByPostId(Integer id);

}
