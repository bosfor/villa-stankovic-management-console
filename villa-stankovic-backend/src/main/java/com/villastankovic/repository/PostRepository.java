package com.villastankovic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.villastankovic.model.Post;

public interface PostRepository extends CrudRepository<Post, Long> {

	@Query("FROM Post ORDER BY id DESC")
	List<Post> findAllDESC();
	
}
