package com.villastankovic.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.villastankovic.model.Request;

@Repository
public interface RequestRepository extends CrudRepository<Request, Integer> {
	
	@Query("FROM Request ORDER BY id DESC")
    List<Request> getAllDESC();
	
	@Query("FROM Request WHERE option=true ORDER BY id DESC")
    List<Request> getHighlighted();
}
