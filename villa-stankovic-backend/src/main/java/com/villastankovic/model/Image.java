package com.villastankovic.model;

import java.util.Arrays;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity
@Table(name = "image")
public class Image {
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "manager_id")
    private Long managerId;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	private String type;
	
    //image bytes can have large lengths so we specify a value
    //which is more than the default length for picByte column
	@Column(name = "picByte")
	private byte[] picByte;
	
	@OneToOne
	@JoinColumn(name="manager_id", insertable = false, updatable = false)
	@JsonIgnoreProperties("image")
    private Manager manager;
	
	
	public Image() {
	}

	public Image(Long id, Long managerId, String name, String type, byte[] picByte, Manager manager) {
		this.id = id;
		this.managerId = managerId;
		this.name = name;
		this.type = type;
		this.picByte = picByte;
		this.manager = manager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getPicByte() {
		return picByte;
	}

	public void setPicByte(byte[] picByte) {
		this.picByte = picByte;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Image [id=" + id + ", managerId=" + managerId + ", name=" + name + ", type=" + type + ", picByte="
				+ Arrays.toString(picByte) + ", manager=" + manager + "]";
	}

}