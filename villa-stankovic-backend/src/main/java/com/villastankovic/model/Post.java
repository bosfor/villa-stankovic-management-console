package com.villastankovic.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name="posts")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Column(name = "manager_id")
    private Long managerId;
    
    private String imgUrl;
    private String description;
    
    @CreatedDate
    private Timestamp createdTimestamp;
    
    @JsonIgnoreProperties({"post", "manager"})
    @OneToMany(mappedBy="post", cascade=CascadeType.ALL)
	private Set<Comment> comments;

    
    @ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="manager_id", insertable = false, updatable = false)
	@JsonIgnoreProperties({"posts", "comments"})
    private Manager manager;
     
	public Post() {
	}

	public Post(Long id, Long managerId, String imgUrl, String description, Timestamp createdTimestamp,
			Set<Comment> comments, Manager manager) {
		this.id = id;
		this.managerId = managerId;
		this.imgUrl = imgUrl;
		this.description = description;
		this.createdTimestamp = createdTimestamp;
		this.comments = comments;
		this.manager = manager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getManagerId() {
		return managerId;
	}

	public void setManagerId(Long managerId) {
		this.managerId = managerId;
	}

	public String getImgUrl() {
		return imgUrl;
	}

	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Timestamp getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Timestamp createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Post [id=" + id + ", managerId=" + managerId + ", imgUrl=" + imgUrl + ", description=" + description
				+ ", createdTimestamp=" + createdTimestamp + ", comments=" + comments + ", manager=" + manager + "]";
	}

		

}