package com.villastankovic.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="comments")
public class Comment {
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column(name="post_id", nullable = false)
	private Integer postId;
	
	@Column (name="content", nullable = false)
	private String content;
	
	@Column(name = "creation_date", updatable = false)
	private Date creationDate;
	
	@Column(name = "manager_id", nullable = false)
	private Integer managerId;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="post_id", insertable = false, updatable = false)
	@JsonIgnoreProperties({"comments", "manager", "image"})
	private Post post;
	
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name="manager_id", insertable = false, updatable = false)
	@JsonIgnoreProperties({"comments", "posts"})
	private Manager manager;

	public Comment() {
	}

	public Comment(Long id, Integer postId, String content, Date creationDate, Integer managerId, Post post,
			Manager manager) {
		this.id = id;
		this.postId = postId;
		this.content = content;
		this.creationDate = creationDate;
		this.managerId = managerId;
		this.post = post;
		this.manager = manager;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getPostId() {
		return postId;
	}

	public void setPostId(Integer postId) {
		this.postId = postId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Integer getManagerId() {
		return managerId;
	}

	public void setManagerId(Integer managerId) {
		this.managerId = managerId;
	}

	public Post getPost() {
		return post;
	}

	public void setPost(Post post) {
		this.post = post;
	}

	public Manager getManager() {
		return manager;
	}

	public void setManager(Manager manager) {
		this.manager = manager;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", postId=" + postId + ", content=" + content + ", creationDate=" + creationDate
				+ ", managerId=" + managerId + ", post=" + post + ", manager=" + manager + "]";
	}


}
