package com.villastankovic.model;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Request {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)	
	private int  id;
	
	private String email;
	private String name;
	private String phone;
	private boolean option;
	private String question;
	private Timestamp time;
	
	public Request() {
	}

	public Request(int id, String email, String name, String phone, boolean option, String question, Timestamp time) {
		this.id = id;
		this.email = email;
		this.name = name;
		this.phone = phone;
		this.option = option;
		this.question = question;
		this.time = time;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public boolean getOption() {
		return option;
	}

	public void setOption(boolean option) {
		this.option = option;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "Request [id=" + id + ", email=" + email + ", name=" + name + ", phone=" + phone + ", option=" + option
				+ ", question=" + question + ", time=" + time + "]";
	}

	
	
}
