package com.villastankovic.model;

import java.sql.Timestamp;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
public class Manager {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long  id;
      
	private String email;
	private String username;
	private String first_name;
	private String last_name;
	private String password;
	
	@CreatedDate
	private Timestamp registration_date;
	private boolean option;
		
	@OneToMany(mappedBy="manager", cascade=CascadeType.ALL)
	@JsonIgnoreProperties({"manager", "comments",  "image"})
	private Set<Post> posts;

	@JsonIgnoreProperties({"manager", "post"})
	@OneToMany(mappedBy = "manager", cascade = CascadeType.ALL)
	private Set<Comment> comments;
	
//	@OneToOne(mappedBy="manager", cascade=CascadeType.ALL)
//    private Image image;

	public Manager() {
	}

	public Manager(Long id, String email, String username, String first_name, String last_name, String password,
			Timestamp registration_date, boolean option, Set<Post> posts, Set<Comment> comments) {
		this.id = id;
		this.email = email;
		this.username = username;
		this.first_name = first_name;
		this.last_name = last_name;
		this.password = password;
		this.registration_date = registration_date;
		this.option = option;
		this.posts = posts;
		this.comments = comments;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getRegistration_date() {
		return registration_date;
	}

	public void setRegistration_date(Timestamp registration_date) {
		this.registration_date = registration_date;
	}

	public boolean isOption() {
		return option;
	}

	public void setOption(boolean option) {
		this.option = option;
	}

	public Set<Post> getPosts() {
		return posts;
	}

	public void setPosts(Set<Post> posts) {
		this.posts = posts;
	}

	public Set<Comment> getComments() {
		return comments;
	}

	public void setComments(Set<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "Manager [id=" + id + ", email=" + email + ", username=" + username + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", password=" + password + ", registration_date=" + registration_date
				+ ", option=" + option + ", posts=" + posts + ", comments=" + comments + "]";
	}

	
}
