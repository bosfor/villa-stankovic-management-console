const baseUri = 'http://ec2-3-85-131-194.compute-1.amazonaws.com';
const port = '8080';

export const environment = {
  production: false,
  environmentName: 'Default Dev Environment',

  requestsUri: `${baseUri}:${port}/requests`,
  allRequestsUri: `${baseUri}:${port}/requests/all`,

  managerUri: `${baseUri}:${port}/manager`,

  postsUri: `${baseUri}:${port}/posts`,

  commentsUri: `${baseUri}:${port}/comments`,
  
  imageUri: `${baseUri}:${port}/image`,

  loginUri: `${baseUri}:${port}/login`,

  url: `${baseUri}:${port}`,
  
};