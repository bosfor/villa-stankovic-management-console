import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms'
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  showMsg: boolean = false;
  msg : any ='';
  
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  registerUser(f: NgForm) {
        this.http.post(environment.managerUri + '/register', f.value).subscribe(
          res=>{
                 console.log(res);
                 this.showMsg= true;
                 setTimeout(() => {
                  this.showMsg= false;
                  },3000);  
                 f.reset();
               },
               error => {
                 console.log("exception ocured"),
                 this.msg=error.error
               })      
      }
}
