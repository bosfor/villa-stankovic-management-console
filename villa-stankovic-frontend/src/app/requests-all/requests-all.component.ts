import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-requests-all',
  templateUrl: './requests-all.component.html',
  styleUrls: ['./requests-all.component.css']
})
export class RequestsAllComponent implements OnInit {

  p: any;
  response: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getRequests();

  }

  getRequests() {
    this.http.get(environment.allRequestsUri)
    .subscribe(
      (response) => {this.response = response; 
      console.log(response);
    })
  }
  deleteRequest(id) {
    if(confirm("Are you sure to delete request with id " + id + " ?")) {
    this.http.delete(`${environment.requestsUri}/${id}`)
    .subscribe(response => {
      console.log("Request is Deleted");
      this.getRequests();
    })
  }
  }

  unhighlight(id, response) {
    this.http.put(`${environment.requestsUri}/unhighlight/${id}`, response)
    .subscribe(response => {
      console.log("Request is unhighlighted");
      this.getRequests();
    })
  }

  highlight(id, response) {
    this.http.put(`${environment.requestsUri}/highlight/${id}`, response)
    .subscribe(response => {
      console.log("Request is highlighted");
      this.getRequests();
    })
  }
 }



