import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

  p: any;
  response: any;
  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getUsers();
  }

  deleteUser(id) {
    if(confirm("Are you sure to delete user with id " + id + " ?")) {
    this.http.delete(environment.managerUri + '/' + id)
    .subscribe(response => {
      console.log("User Deleted");
      this.getUsers();
    })
  }
  }

  getUsers() {
    this.http.get(environment.managerUri + '/all')
    .subscribe(
      (response) => {this.response = response; 
      console.log(response);
    })
  }

  unadmin(id, response) {
    this.http.put(environment.managerUri + '/unadmin/' + id, response)
    .subscribe(response => {
      console.log("User is not Admin");
      this.getUsers();
    })
  }

  admin(id, response) {
    this.http.put(environment.managerUri + '/admin/' + id, response)
    .subscribe(response => {
      console.log("User is Admin");
      this.getUsers();
    })
  }

}