import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Manager } from '../model/manager';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient) { }

  public logginFromRemote(manager :Manager):Observable<any>{
    console.log(manager.email);
    console.log(manager.password);
    const headers = new HttpHeaders({ Authorization: 'Basic ' + btoa(manager.email + ':' + manager.password) });
    return this.httpClient.post<any>(environment.loginUri, manager, { headers }).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', manager.email);
          let authString = 'Basic ' + btoa(manager.email + ':' + manager.password);
          sessionStorage.setItem('basicauth', authString);
          return userData;
          }
        )
        );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('username')
  }
}