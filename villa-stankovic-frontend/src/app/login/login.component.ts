import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { User } from '../model/user';
import { Manager } from '../model/manager';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = '';
  password = '';
  invalidLogin = false;
  msg = '';
  // user = new User();
  manager = new Manager();

  constructor(
    private router: Router,
    private loginservice: AuthenticationService
    ) { }

    ngOnInit(): void {
    }

    loginUser(){
      this.loginservice.logginFromRemote(this.manager).subscribe(
        data => {
          console.log("response recived"),
          this.router.navigate(['/profile'])
        },
        error => {
          console.log("exception ocured"),
          this.msg="Bad Credentials, please enter valid email and password"
        })
    }
}