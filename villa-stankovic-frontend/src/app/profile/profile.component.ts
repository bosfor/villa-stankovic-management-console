import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, AfterViewInit {
  userDisplayName:any = '';
  response: any;

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any = 2;
  showMsg: boolean = false;

  constructor(
    private http: HttpClient,
    private router: Router,
    ) { }


  ngAfterViewInit() {
    this.getImage(); 
   }

  ngOnInit() {
    this.getData();  
  }

  getData() {
    this.http.get(environment.managerUri+ '/' + sessionStorage.getItem('username'))
    .subscribe(
      (response) => {
        this.response = response; 
        console.log(response);
    })
  }

  updateProfile(update) {
    this.http.put(environment.managerUri + '/update/' + this.response.id, update)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          console.log(res);
          this.showMsg= true;
          setTimeout(() => {
            this.showMsg= false;
        },3000);
        }     
      );
  }

 //Gets called when the user selects an image
  onFileChanged(event) {
    this.selectedFile = event.target.files[0]; //Select File
  }

    //Gets called load page
    getImage() {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.http.get(environment.imageUri + '/get/' + this.response.id)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

    //Gets called when the user clicks on submit to upload the image
    onUpload() {
      console.log(this.selectedFile); 
      //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
      const uploadImageData = new FormData();
      uploadImageData.append('imageFile', this.selectedFile, this.response.id);  
      //Make a call to the Spring Boot Application to save the image
      this.http.post(environment.imageUri + '/upload', uploadImageData, { observe: 'response' })
        .subscribe((response) => {
          if (response.status === 200) {
            this.message = 'Image uploaded successfully';
            this.getImage();
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
        );
    }


    onUpdate() {
      console.log(this.selectedFile); 
      //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
      const uploadImageData = new FormData();
      uploadImageData.append('imageFile', this.selectedFile, this.response.id);  
      //Make a call to the Spring Boot Application to save the image
      this.http.put(environment.imageUri + '/update', uploadImageData, { observe: 'response' })
        .subscribe((response) => {
          if (response.status === 200) {
            this.message = 'Image uploaded successfully';
            this.getImage();
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
        );
    }
}
