import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})
export class CreatePostComponent implements OnInit {
  showMsg: boolean = false;
  response: any;

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.getData();  
  }

  getData() {
    this.http.get(environment.managerUri + '/' + sessionStorage.getItem('username'))
    .subscribe(
      (response) => {
        this.response = response;
        console.log(response);
    })
  }

  createPost(f: NgForm){
    this.http.post(environment.postsUri, f.value)
    .subscribe(
      res => {
        console.log(res)
        this.showMsg= true;
             setTimeout(() => {
              this.showMsg= false;
              },3000);  
             f.reset();
      })
  }

}
