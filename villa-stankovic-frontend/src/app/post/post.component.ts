import { Component, OnInit, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit, AfterViewInit {
  retrievedImage: any;
  base64Data: any;
  retrieveResponse: any;
  response: any = [];
  res: Object;
  id: any ;
  msg : any ='';
  loggedUser: any;
  comments: Object;

  constructor(
    private http: HttpClient
    ) { }

  ngAfterViewInit() {
    // this.getImage(); 
    this.load(this.id);
   }
  
  ngOnInit() {
    this.getData(); 
    this.getPosts();   
    this.getComments();
  }

  getComments() {
    this.http.get(environment.commentsUri + '/all')
    .subscribe((cresponse) => {
      this.comments = cresponse;
      this.getData();
      this.getPosts();
      console.log(cresponse);
    });
  }

  getData() {
    this.http.get(environment.managerUri + '/' + sessionStorage.getItem('username'))
    .subscribe(
      (response) => {
        this.loggedUser = response; 
        console.log(response);
    })
  }

  getPosts() {
    this.http.get(environment.postsUri)
    .subscribe((response) => {
      this.response = response;
      console.log(response);
    });
  }

  load(id) {
    this.http.get(environment.imageUri + '/get/' + id)
      .subscribe(
        res => {
          this.retrieveResponse = res;
          this.base64Data = this.retrieveResponse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

  newComment(f: NgForm) {
    this.http.post(environment.commentsUri + '/create', f.value).subscribe(
      res=>{
             console.log(res);
             f.reset();
            //  this.getPosts();
             this.getComments();
           },
           error => {
             console.log("exception ocured"),
             this.msg=error.error
           });      
  }

  deleteComment(id){
    this.http.delete(environment.commentsUri + '/' + id)
    .subscribe(response => {
      console.log("Comment is Deleted");
      this.getComments();
    })

  }

  deletePost(id){
    this.http.delete(environment.postsUri + '/' + id)
    .subscribe(() => {
      console.log("Post is Deleted");
      this.getPosts();
    })

  }
  // getImage() {
  //   this.http.get('http://localhost:8080/image/get/' + this.id)
  //     .subscribe(
  //       res => {
  //         this.retrieveResponse = res;
  //         this.base64Data = this.retrieveResponse.picByte;
  //         this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
  //       }
  //     );
  // }
}


