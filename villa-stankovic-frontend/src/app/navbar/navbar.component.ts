import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  response: any;

  constructor(
    private loginService:AuthenticationService,
    private http: HttpClient,
    ) { }

  ngOnInit() {
    this.getData();  
  }

  getData() {
    this.http.get('http://localhost:8080/manager/' + sessionStorage.getItem('username'))
    .subscribe(
      (response) => {
        this.response = response; 
        console.log(response);
    })
  }
  hideNavbar() : boolean{
   return this.loginService.isUserLoggedIn();
  }

  
}
