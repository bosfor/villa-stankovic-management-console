import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { ProfileComponent } from './profile/profile.component';
import { RequestsAllComponent } from './requests-all/requests-all.component';
import { RequestsHighlightedComponent } from './requests-highlighted/requests-highlighted.component';
import { ManagerComponent } from './manager/manager.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGaurdService } from './service/auth-gaurd.service';
import { PostComponent } from './post/post.component';
import { CreatePostComponent } from './create-post/create-post.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate:[AuthGaurdService] },
  { path: 'profile', component: ProfileComponent, canActivate:[AuthGaurdService] },
  { path: 'register', component: RegisterComponent, canActivate:[AuthGaurdService] },
  { path: 'requests', component: RequestsAllComponent, canActivate:[AuthGaurdService] },
  { path: 'highlighted', component: RequestsHighlightedComponent, canActivate:[AuthGaurdService] },
  { path: 'users', component: ManagerComponent, canActivate:[AuthGaurdService] },
  { path: 'posts', component: PostComponent, canActivate:[AuthGaurdService] },
  { path: 'create', component: CreatePostComponent, canActivate:[AuthGaurdService] },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
